package HomeWorks.lesson7.task1_2;

public class User {
    public String fullName;
    public int age;

    public User(String fullName, int age) {
        this.fullName = fullName;
        this.age = age;
    }
}
