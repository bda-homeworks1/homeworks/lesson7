package HomeWorks.lesson7.task6;

public class User {
    private String name;
    private int age;

    public String adiGetir() {
        return name;
    }

    public void adDeyis(String name) {
        this.name = name;
    }

    public int yasiGetir() {
        return age;
    }

    public void yasDeyis(int age) {
        this.age = age;
    }
}
