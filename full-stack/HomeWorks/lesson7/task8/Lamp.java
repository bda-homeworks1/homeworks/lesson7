package HomeWorks.lesson7.task8;

public class Lamp {
    public boolean light = false;

    public  void yandir(){
        if(light==false){
            light = true;
            System.out.println("lampa yandirildi");
        }else{
            System.out.println("lampa onsuz yanilidi");
        }
    }

    public void sondur(){
        if(light == true){
            light=false;
            System.out.println("lampa sonduruldu");
        }else{
            System.out.println("onsuz lampa sonuludu");
        }
    }

    public void veziyyetulLampa(){
        System.out.println("lampa:" + light);
    }


}
