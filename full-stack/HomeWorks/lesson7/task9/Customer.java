package HomeWorks.lesson7.task9;

public class Customer {
    public String name;
    public int cash;


    public Customer(String name, int cash) {
        this.name = name;
        this.cash = cash;
    }

    public void transfer(Customer kocuren, Customer kocurulen, int mebleg){
        kocuren.cash -= mebleg;
        kocurulen.cash+=mebleg;
    }

}
